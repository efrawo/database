<?php

namespace ef\database\connection;


use ef\database\config\_AConfig;

class Sybase extends _AConnection
{
    protected function getDSN(_AConfig $config)
    {
        if (isset($config->charset)) {
            $this->addCommand("SET NAMES '" . $config->charset . "'");
        }

        return 'dblib:host=' . $config->server . (isset($config->port) ? ':' . $config->port : '') . ';dbname=' . $config->dbName;
    }
}