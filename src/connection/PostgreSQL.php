<?php

namespace ef\database\connection;


use ef\database\config\_AConfig;

class PostgreSQL extends _AConnection
{
    protected function getDSN(_AConfig $config)
    {
        if (isset($config->charset)) {
            $this->addCommand("SET NAMES '" . $config->charset . "'");
        }

        return 'pgsql:host=' . $config->server . (isset($config->port) ? ';port=' . $config->port : '') . ';dbname=' . $config->dbName;
    }
}