<?php

namespace ef\database\connection;

use ef\database\config\_AConfig;

use Exception;
use PDO;
use PDOException;

abstract class _AConnection extends PDO
{
    /** @var array */
    private $commands = array();

    abstract protected function getDSN(_AConfig $config);

    /**
     * @param _AConfig $config
     * @throws Exception
     * @throws PDOException
     */
    public function __construct(_AConfig $config)
    {
        if (!$config->validate()) {
            throw new Exception('Config is not valid');
        }

        return parent::__construct(
            $this->getDSN($config),
            isset($config->username) ? $config->username : null,
            isset($config->password) ? $config->password : null,
            $config->option
        );
    }

    /**
     * @param string $command
     */
    protected function addCommand($command)
    {
        $this->commands[] = $command;
    }

    public function init()
    {
        foreach ($this->commands as $value) {
            $this->exec($value);
        }
    }

}
