<?php

namespace ef\database\connection;

use ef\database\config\_AConfig;
use ef\database\config\General;
use ef\database\config\UnixSocket;

class MySQL extends _AConnection
{
    protected function getDSN(_AConfig $config)
    {
        // Make MySQL using standard quoted identifier
        $this->addCommand('SET SQL_MODE=ANSI_QUOTES');

        if (isset($config->charset)) {
            $this->addCommand("SET NAMES '" . $config->charset . "'");
        }

        if ($config instanceof UnixSocket) {
            return 'mysql:unix_socket=' . $config->socket . ';dbname=' . $config->dbName;
        } else {
            /** @var General $config */
            return 'mysql:host=' . $config->server . (isset($config->port) ? ';port=' . $config->port : '') . ';dbname=' . $config->dbName;
        }
    }
}