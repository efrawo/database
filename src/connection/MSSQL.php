<?php

namespace ef\database\connection;


use ef\database\config\_AConfig;

class MSSQL extends _AConnection
{
    protected function getDSN(_AConfig $config)
    {
        // Keep MSSQL QUOTED_IDENTIFIER is ON for standard quoting
        $this->addCommand('SET QUOTED_IDENTIFIER ON');

        if (isset($config->charset)) {
            $this->addCommand("SET NAMES '" . $config->charset . "'");
        }

        return strstr(PHP_OS, 'WIN') ?
            'sqlsrv:server=' . $config->server . (isset($config->port) ? ',' . $config->port : '') . ';database=' . $config->dbName :
            'dblib:host=' . $config->server . (isset($config->port) ? ':' . $config->port : '') . ';dbname=' . $config->dbName;
    }
}