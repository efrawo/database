<?php

namespace ef\database\connection;

use ef\database\config\_AConfig;


class SQLite extends _AConnection
{
    protected function getDSN(_AConfig $config)
    {
        /** @var \ef\database\config\SQLite $config */
        return 'sqlite:' . $config->dbFile;
    }
}