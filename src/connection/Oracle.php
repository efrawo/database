<?php

namespace ef\database\connection;


use ef\database\config\_AConfig;

class Oracle extends _AConnection
{
    protected function getDSN(_AConfig $config)
    {
        $dbname = $config->server ?
            '//' . $config->server . (isset($config->port) ? ':' . $config->port : ':1521') . '/' . $config->dbName :
            $config->dbName;

        return 'oci:dbname=' . $dbname . ($config->charset ? ';charset=' . $config->charset : '');
    }
}