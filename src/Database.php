<?php

namespace ef\database;


use ef\database\config\_AConfig;
use ef\database\config\General;
use ef\database\connection\_AConnection;
use ef\database\connection\MariaDB;
use ef\database\connection\MSSQL;
use ef\database\connection\MySQL;
use ef\database\connection\Oracle;
use ef\database\connection\PostgreSQL;
use ef\database\connection\SQLite;
use ef\database\connection\Sybase;

use Exception;
use InvalidArgumentException;
use PDO;

/**
 * Class Database
 */
class Database
{
    /** @var PDO */
    protected $connection;

    /** @var \ef\database\config\_AConfig */
    protected $config = null;

    /** @var array */
    protected $logs = array();

    /**
     * @param array|_AConfig $config
     * @throws \Exception
     */
    public function __construct($config)
    {
        if (is_array($config)) {
            $config = new General($config);
        }

        if (!($config instanceof _AConfig)) {
            throw new InvalidArgumentException('Parameter config must be an array or instance of _AConfig');
        }

        $this->config = $config;

        if (!$this->config->validate()) {
            throw new Exception('Config is not valid');
        }

        $this->connection = $this->createPDO();
        $this->connection->init();
    }

    /**
     * @param string $query
     * @param mixed $params
     * @return bool|\PDOStatement
     */
    public function query($query, $params = null)
    {
        if (!is_array($params) && !is_null($params)) {
            $params = array($params);
        }

        if (isset($params)) {
            $statement = $this->connection->prepare($query);

            if ($this->config->debug) {
                echo $query . ' - ' . implode(',', $params);
            }

            $this->logs[] = $query . ' - ' . implode(',', $params);

            if ($statement->execute($params)) {
                return $statement;
            } else {
                return false;
            }

        } else {
            if ($this->config->debug) {
                echo $query;
            }

            $this->logs[] = $query;

            return $this->connection->query($query);
        }
    }

    /**
     * @param string $query
     * @param mixed $params
     * @return int
     */
    public function exec($query, $params = null)
    {
        if (!is_array($params) && !is_null($params)) {
            $params = array($params);
        }

        if (isset($params)) {
            $statement = $this->connection->prepare($query);

            if ($this->config->debug) {
                echo $query . ' - ' . implode(',', $params);
            }

            $this->logs[] = $query . ' - ' . implode(',', $params);

            $statement->execute($params);

            return $statement->rowCount();

        } else {
            if ($this->config->debug) {
                echo $query;
            }

            $this->logs[] = $query;

            return $this->connection->exec($query);
        }
    }

    /**
     * @return \ef\database\Query
     */
    public function createQuery()
    {
        return new Query($this);
    }

    /**
     * @return string
     */
    public function lastInsertId()
    {
        return $this->connection->lastInsertId();
    }

    /**
     * @return array
     */
    public function error()
    {
        return $this->connection->errorInfo();
    }

    /**
     * @return mixed
     */
    public function lastQuery()
    {
        return end($this->logs);
    }

    /**
     * @return array
     */
    public function log()
    {
        return $this->logs;
    }

    /**
     * @return array
     */
    public function info()
    {
        $output = array(
            'server' => 'SERVER_INFO',
            'driver' => 'DRIVER_NAME',
            'client' => 'CLIENT_VERSION',
            'version' => 'SERVER_VERSION',
            'connection' => 'CONNECTION_STATUS',
        );

        foreach ($output as $key => $value) {
            $output[$key] = $this->connection->getAttribute(constant('PDO::ATTR_' . $value));
        }

        return $output;
    }

    /**
     * @param $string
     * @return string
     */
    public function quote($string)
    {
        return $this->connection->quote($string);
    }

    /**
     * @param $callback
     * @return bool
     * @throws Exception
     */
    public function transaction($callback)
    {
        if (is_callable($callback)) {
            $this->connection->beginTransaction();

            $result = $callback($this);

            if ($result === false) {
                $result = $this->connection->rollBack();

                if ($result === false) {
                    throw new Exception('CRITICAL - a rollback failed');
                }

                return false;
            } else {
                return $this->connection->commit();
            }
        } else {
            return false;
        }
    }

    /**
     * @return _AConnection
     */
    private function createPDO()
    {
        switch ($this->config->dbType) {
            case 'mariadb':
                return new MariaDB($this->config);
            case 'mysql':
                return new MySQL($this->config);
            case 'pgsql':
                return new PostgreSQL($this->config);
            case 'sybase':
                return new Sybase($this->config);
            case 'oracle':
                return new Oracle($this->config);
            case 'mssql':
                return new MSSQL($this->config);
            case 'sqlite':
                return new SQLite($this->config);
            default:
                throw new InvalidArgumentException('Config is not valid');
        }
    }
}
