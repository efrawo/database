<?php

namespace ef\database;

/**
 * Class DatabaseHelper
 */
class DatabaseHelper extends Database
{
    /**
     * @param string $table
     * @param null|array|string $columns
     * @param null|string $where
     * @param null|array $whereParams
     * @param null|int $limit
     * @return array|false
     */
    public function getAll($table, $columns = null, $where = null, $whereParams = null, $limit = null)
    {
        $query = $this->createQuery();

        $query->from($table);

        if (isset($columns)) {
            if (is_array($columns)) {
                $query->select(implode(',', $columns));
            } else {
                $query->select($columns);
            }
        }

        if ((isset($where) && strlen(trim($where)) > 0) || isset($whereParams)) {
            $query->where($where, $whereParams);
        }

        if (isset($limit)) {
            $query->limit($limit);
        }

        return $query->getAll();
    }

    /**
     * @param string $table
     * @param null|array|string $columns
     * @param null|string $where
     * @param null|array $whereParams
     * @return array|false
     */
    public function getRow($table, $columns = null, $where = null, $whereParams = null)
    {
        $query = $this->createQuery();

        $query->from($table);

        if (isset($columns)) {
            if (is_array($columns)) {
                $query->select(implode(',', $columns));
            } else {
                $query->select($columns);
            }
        }

        if ((isset($where) && strlen(trim($where)) > 0) || isset($whereParams)) {
            $query->where($where, $whereParams);
        }

        return $query->getRow();
    }

    /**
     * @param string $table
     * @param null|string $column
     * @param null|string $where
     * @param null|array $whereParams
     * @return false|mixed|null
     */
    public function getOne($table, $column = null, $where = null, $whereParams = null)
    {
        $query = $this->createQuery();

        $query->from($table);

        if (isset($columns)) {
            $query->select($column);
        }

        if ((isset($where) && strlen(trim($where)) > 0) || isset($whereParams)) {
            $query->where($where, $whereParams);
        }

        return $query->getOne($column);
    }

    /**
     * @param string $table
     * @param array|object $data
     * @return bool|string
     */
    public function insert($table, $data)
    {
        $result = $this->executeBySyntax('INSERT INTO', $table, $data);

        if ($result > 0) {
            return $this->lastInsertId();
        }

        return false;
    }

    /**
     * @param string $table
     * @param array|object $data
     * @param null|string|array $where
     * @param null|mixed|array $whereParams
     * @return int
     */
    public function update($table, $data, $where = null, $whereParams = null)
    {
        return $this->executeBySyntax('UPDATE', $table, $data, $where, $whereParams);
    }

    /**
     * @param string $table
     * @param null|string|array $where
     * @param null|mixed|array $whereParams
     * @return int
     */
    public function delete($table, $where = null, $whereParams = null)
    {
        $stmt = $this->exec('DELETE FROM ' . $table . $this->buildWhere($where), $whereParams);

        return $stmt;
    }

    /**
     * @param string $table
     * @param array|object $data
     * @return int
     */
    public function replace($table, $data)
    {
        return $this->executeBySyntax('REPLACE', $table, $data);
    }

    /**
     * Perform INSERT, UPDATE, REPLACE
     *
     * @param string $syntax
     * @param string $table
     * @param array|object $data
     * @param null|string|array $where
     * @param null|mixed|array $whereParams
     * @return int
     */
    private function executeBySyntax($syntax, $table, $data, $where = null, $whereParams = null)
    {
        if (!is_null($where) && !is_array($where)) {
            $where = array($where);
        }
        if (is_object($data)) {
            $data = (array)$data;
        }

        // support for scalar param
        if (!isset($whereParams)) {
            $whereParams = array();
        } else if (!is_array($whereParams)) {
            $whereParams = array($whereParams);
        }

        $sql = $this->getSetSql($syntax, $table, $data, $where);

        return $this->exec($sql, array_merge(array_values($data), $whereParams));
    }

    /**
     * @param string $syntax INSERT, UPDATE, REPLACE
     * @param string $table
     * @param array|object $data
     * @param null|string|array $where
     * @return string
     */
    private function getSetSql($syntax, $table, $data, $where = null)
    {
        $columns = array();
        foreach (array_keys($data) as $column) {
            $columns[] = "`" . $column . "` = ?";
        }
        $columns = implode(", ", $columns);

        return $syntax . "`$table` SET " . $columns . $this->buildWhere($where);
    }

    /**
     * Build where statement for SQL query
     *
     * @param mixed $where
     * @param string $operand AND | OR
     * @return string
     */
    private function buildWhere($where, $operand = "AND")
    {
        if (empty($where)) {
            return "";
        }
        if (is_array($where)) {
            $wheres = array();
            foreach ($where as $k => $w) {
                $wheres[] = "(" . $w . ")";
            }
            $where = implode(" $operand ", $wheres);
        }

        return " WHERE " . $where;
    }

    /**
     * @param string $table
     * @param string $column
     * @param null|string $where
     * @param null|array $whereParams
     * @return bool|mixed|null
     */
    public function count($table, $column, $where = null, $whereParams = null)
    {
        return $this->countOne('COUNT', $table, $column, $where, $whereParams);
    }

    /**
     * @param string $table
     * @param string $column
     * @param null|string $where
     * @param null|array $whereParams
     * @return bool|mixed|null
     */
    public function max($table, $column, $where = null, $whereParams = null)
    {
        return $this->countOne('MAX', $table, $column, $where, $whereParams);
    }

    /**
     * @param string $table
     * @param string $column
     * @param null|string $where
     * @param null|array $whereParams
     * @return bool|mixed|null
     */
    public function min($table, $column, $where = null, $whereParams = null)
    {
        return $this->countOne('MIN', $table, $column, $where, $whereParams);
    }

    /**
     * @param string $table
     * @param string $column
     * @param null|string $where
     * @param null|array $whereParams
     * @return bool|mixed|null
     */
    public function avg($table, $column, $where = null, $whereParams = null)
    {
        return $this->countOne('AVG', $table, $column, $where, $whereParams);
    }

    /**
     * @param string $table
     * @param string $column
     * @param null|string $where
     * @param null|array $whereParams
     * @return bool|mixed|null
     */
    public function sum($table, $column, $where = null, $whereParams = null)
    {
        return $this->countOne('SUM', $table, $column, $where, $whereParams);
    }

    /**
     * @param string $method
     * @param string $table
     * @param string $column
     * @param null|string $where
     * @param null|array $whereParams
     * @return bool|mixed|null
     */
    private function countOne($method, $table, $column, $where = null, $whereParams = null)
    {
        $query = $this->createQuery();

        $query->from($table);

        if ((isset($where) && strlen(trim($where)) > 0) || isset($whereParams)) {
            $query->where($where, $whereParams);
        }

        return $query->getOne($method . '(' . $column . ')');
    }
}
