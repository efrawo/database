<?php

namespace ef\database\config;

use InvalidArgumentException;

abstract class _AConfig
{
    public $dbType;

    public $dbName;

    public $charset;

    // optional
    public $prefix;
    public $option = array();

    /** @var bool */
    public $debug;

    /** @return array */
    abstract protected function getValidDatabaseType();

    public function __construct(array $options = array())
    {
        foreach ($options as $key => $value) {
            $this->$key = $value;
        }
    }

    /**
     * @return bool
     */
    public function validate()
    {
        if (!isset($this->dbType)) {
            throw new InvalidArgumentException('Parameter dbType missing');
        }

        $this->dbType = strtolower($this->dbType);

        if (!in_array($this->dbType, $this->getValidDatabaseType())) {
            throw new InvalidArgumentException('This database type is not supported');
        }

        if (!isset($this->dbName)) {
            throw new InvalidArgumentException('Parameter dbName missing');
        }
        if (!isset($this->charset)) {
            throw new InvalidArgumentException('Parameter charset missing');
        }

        return true;
    }
}