<?php

namespace ef\database\config;

use InvalidArgumentException;

/**
 * Class UnixSocket
 *
 * For MySQL or MariaDB with unix_socket
 */
class UnixSocket extends General
{
    public $socket;

    public function validate()
    {
        if (!isset($this->socket)) {
            throw new InvalidArgumentException('Parameter socket missing');
        }

        return parent::validate();
    }
}