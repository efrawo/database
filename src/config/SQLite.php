<?php

namespace ef\database\config;

use InvalidArgumentException;

/**
 * Class SQLite
 *
 * SQLite config
 */
class SQLite extends _AConfig
{
    public $dbFile;


    protected function getValidDatabaseType()
    {
        return array(
            'sqlite',
        );
    }

    public function validate()
    {
        if (!isset($this->dbFile)) {
            throw new InvalidArgumentException('Parameter dbFile missing');
        }

        return parent::validate();
    }
}