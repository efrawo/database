<?php

namespace ef\database\config;

use InvalidArgumentException;

/**
 * Class General
 *
 * For MySQL, MariaDB, MSSQL, Sybase, PostgreSQL, Oracle
 */
class General extends _AConfig
{
    public $server;
    public $username;
    public $password;

    // optional
    public $port;


    protected function getValidDatabaseType()
    {
        return array(
            'mariadb',
            'mysql',
            'pgsql',
            'sybase',
            'oracle',
            'mssql',
        );
    }

    public function validate()
    {
        if (!isset($this->server)) {
            throw new InvalidArgumentException('Parameter server missing');
        }
        if (!isset($this->username)) {
            throw new InvalidArgumentException('Parameter username missing');
        }
        if (!isset($this->password)) {
            throw new InvalidArgumentException('Parameter password missing');
        }

        if (isset($this->port) && !is_int($this->port)) {
            throw new InvalidArgumentException('Parameter port is not a valid integer');
        }

        return parent::validate();
    }
}